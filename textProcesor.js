let TEXT = "hola, este es el texto por defecto";

class TextProcessor {
  wordToFind(aText) {
    const isFound = TEXT.includes(aText);
    
    return isFound;
  }

  setText(textToSet){
    TEXT = textToSet
  }
  getText(){
    return TEXT
  } 
}

describe("TextProcessor", () => {
  describe("wordToFind ", () => {
    
    it("returns true if a given word is found in a text ", () => {
      // Arrange
      const textProcessor = new TextProcessor();
      const wordToFind = "hola";
      
      // Act
      const wordExists = textProcessor.wordToFind(wordToFind);
      
      // Assert
      expect(wordExists).toBe(true);
    });
    
    it("returns false if a given word is not found in a text", () => {
      const textProcessor = new TextProcessor();
      const wordToFind = "cachopo";
      
      const wordExists = textProcessor.wordToFind(wordToFind);
      
      expect(wordExists).toBe(false);
    });
  })
    
  it("sets a given text", () => {
    const textProcessor = new TextProcessor()
    const textToSet = "Soy el texto a setear"

    textProcessor.setText(textToSet)
    const settedText = textProcessor.getText() 



    expect(settedText).toBe(textToSet);
  })
});
